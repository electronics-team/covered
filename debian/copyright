Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: covered
Upstream-Contact: Trevor Williams <phase1geo@gmail.com>
Source: http://covered.sourceforge.net

Files: *
Copyright: 2006-2009, Trevor Williams <phase1geo@gmail.com>
License: GPL-2+

Files: src/cexcept.h
Copyright: 2000, Adam M. Costello and Cosmin Truta
License: other
    Everyone is hereby granted permission to do whatever they like with this
    file, provided that if they modify it they take reasonable steps to avoid
    confusing or misleading people about the authors, version, and terms of use
    of the derived file. The copyright holders make no guarantees about the
    correctness of this file, and are not responsible for any damage resulting
    from its use.

Files: src/lxt2_read.*
Copyright: 2003-2004, Tony Bybell
License: MIT
 Permission is hereby granted, free of charge, to any person obtaining a
 copy of this software and associated documentation files (the "Software"),
 to deal in the Software without restriction, including without limitation
 the rights to use, copy, modify, merge, publish, distribute, sublicense,
 and/or sell copies of the Software, and to permit persons to whom the
 Software is furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 DEALINGS IN THE SOFTWARE.

Files: debian/*
Copyright: 2009-2011,2013-2015,2023-2024 أحمد المحمودي (Ahmed El-Mahmoudy) <aelmahmoudy@users.sourceforge.net>
License: GPL-2+

License: GPL-2+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 USA.
 .
 On Debian systems, the complete text of the GNU GPL2 licenses can be found at
 `/usr/share/common-licenses/GPL-2'.
